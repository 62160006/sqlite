/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phai.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phai
 */
public class CreateTable {

    public static void main(String[] args) {
        Connection con = null;
        String dbName = "user.db";
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            stmt = con.createStatement();
            String sql = "Create Table Company"+
                    "(ID INT PRIMARY KEY NOT NULL, "+
                    "NAME TEXT NOT NULL,"+
                    "AGE INT NOT NULL,"+
                    "ADDRESS CHAR(50),"+
                    "SALARY REAL)";
            stmt.executeUpdate(sql);
            stmt.close();
            con.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Not found library");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Connect database " + dbName + " failed");   
            System.exit(0);
        }
        
        
    }
}
