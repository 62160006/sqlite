/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phai.sqliteproject;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phai
 */
public class ConnectDatabase {
    public static void main(String[] args) {
        Connection con = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+dbName);
        } catch (ClassNotFoundException ex) {
            System.out.println("Library not found");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Connect database " + dbName + " failed");   
            System.exit(0);
        }
        System.out.println("Connect database " + dbName + " sucessfully");
    }
}
